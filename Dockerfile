FROM alpine:latest

RUN apk add --no-cache \
    bash \
    bind \
    bind-tools

WORKDIR /zones
